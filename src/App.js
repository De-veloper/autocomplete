import AutoComplete from "./pages/AutoComplete";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import React from "react";
function App() {
    return (
        <div className="App">
            <center>
                <h2>Auto Complete</h2>
            </center>
            <AutoComplete />
        </div>
    );
}

export default App;
