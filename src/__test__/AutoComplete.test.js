import { render, screen, fireEvent } from "@testing-library/react";
import { toBeInTheDocument } from "@testing-library/jest-dom";
//import DATA from "./../mockdata.js";
import React from "react";
//import Input from "./../components/Input";
import AutoComplete from "./../pages/AutoComplete";

test("renders AutoComplete", () => {
    const { getByPlaceholderText } = render(<AutoComplete />);
    const inputElem = getByPlaceholderText(/Name/i);
    expect(inputElem).toBeInTheDocument();
});

test("test onchnage", () => {
    const { getByPlaceholderText } = render(<AutoComplete />);

    const inputElem = getByPlaceholderText(/Name/i); //screen.getByText(/Name/i);

    const query = "A";

    expect(inputElem.value).toEqual("");

    fireEvent.change(inputElem, { target: { value: query } });

    //expect(inputElem.onchange).toBeCalledTimes(1);

    expect(inputElem.value).toEqual(query);
});

test("test on click", () => {
    const { getByPlaceholderText } = render(<AutoComplete />);

    const inputElem = getByPlaceholderText(/Name/i); //screen.getByText(/Name/i);

    const query = "A";

    expect(inputElem.value).toEqual("");

    fireEvent.change(inputElem, { target: { value: query } });

    const names = screen.getByText(/Aria/i);

    const leftclick = { buton: 0 };

    fireEvent.click(names, leftclick);

    expect(inputElem.value).toEqual("Aria");
});

describe("Match names", () => {
    test("test dropdown list with matched first letter of name", () => {
        const mData = ["Aria", "Alex", "Alan", "Ace", "Bob", "Brian"];

        const { getByPlaceholderText } = render(<AutoComplete />);

        const inputElem = getByPlaceholderText(/Name/i); //screen.getByText(/Name/i);

        const query = "B";

        expect(inputElem.value).toEqual("");

        fireEvent.change(inputElem, { target: { value: query } });

        let reg = new RegExp("^" + query + "", "");

        let getMatch = mData.filter((e) => {
            return reg.test(e) ? e : "";
        });

        if (getMatch.length > 0) {
            getMatch.forEach((e) => {
                const name = screen.getByText(e);
                expect(name).toBeInTheDocument();
            });
        }
    });

    test("test dropdown list with matched name", () => {
        const { getByPlaceholderText } = render(<AutoComplete />);

        const inputElem = getByPlaceholderText(/Name/i); //screen.getByText(/Name/i);

        const query = "Al";

        expect(inputElem.value).toEqual("");

        fireEvent.change(inputElem, { target: { value: query } });

        const name1 = screen.getByText(/Alex/i);
        const name2 = screen.getByText(/Alan/i);

        expect(name1).toBeInTheDocument();
        expect(name2).toBeInTheDocument();
    });
});
