import React, { useState } from "react";
import DATA from "./../mockdata.js";
import Input from "./../components/Input";

const AutoComplete = () => {
    const mainData = DATA;
    const [filterArr, setFilterArr] = useState([]);
    const [item, setItem] = useState("");
    const clickEvent = (e) => {
        let getData = e.target.childNodes[0].data;
        setItem(getData);
        setFilterArr([]);
    };
    const findMatch = (input) => {
        let reg = new RegExp("^" + input + "", "");
        let getMatch = mainData.filter((e) => {
            return reg.test(e) ? e : "";
        });
        setFilterArr([...getMatch]);
    };
    const changeHandler = (e) => {
        let getData = e.target.value;
        findMatch(getData);
        setItem(getData);
    };
    return (
        <center>
            <Input
                placeholder="Name"
                value={item ? item : ""}
                onChange={(e) => {
                    changeHandler(e);
                }}
            />
            <br />
            {filterArr.length > 0 && item !== ""
                ? filterArr.map((e, i) => {
                      return (
                          <div key={i} onClick={(e) => clickEvent(e)}>
                              {e}
                          </div>
                      );
                  })
                : ""}
        </center>
    );
};

export default AutoComplete;
